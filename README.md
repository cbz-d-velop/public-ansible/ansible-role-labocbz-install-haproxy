# Ansible role: labocbz.install_haproxy

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![Testing Method](https://img.shields.io/badge/Testing%20Method-Ansible%20Molecule-blueviolet)
![Testing Driver](https://img.shields.io/badge/Testing%20Driver-docker-blueviolet)
![Language Status](https://img.shields.io/badge/language-Ansible-red)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Author](https://img.shields.io/badge/Author-Lord%20Robin%20Crombez-blue)

## Description

![Tag: Ansible](https://img.shields.io/badge/Tech-Ansible-orange)
![Tag: Debian](https://img.shields.io/badge/Tech-Debian-orange)
![Tag: Ubuntu](https://img.shields.io/badge/Tech-Ubuntu-orange)
![Tag: HAProxy](https://img.shields.io/badge/Tech-HAProxy-orange)

An Ansible role to install and configure a HAProxy server on your host.

The Ansible role installs HAProxy, a powerful load balancer, on the target system. It provides a seamless installation process using packages, ensuring a reliable and consistent setup. While the role offers flexibility to configure custom paths for installation, it is advisable to utilize the default paths for a smoother experience.

Along with installation, the role allows administrators to fine-tune the HAProxy configuration by customizing various aspects such as timeout values, log settings, and different options. The role also facilitates the setup of specific error pages for distinct HTTP status codes, enhancing the user experience during error scenarios.

One notable feature of the role is its ability to handle the removal of old and outdated HAProxy configurations. When deploying HAProxy configurations that might change over time, previous configurations may become obsolete. To maintain a clean and up-to-date setup, the role effectively manages the removal of these old configurations, ensuring the system operates with the latest and most relevant settings.

Additionally, the role provides an option to activate the web interface, which allows administrators to visualize load balancer configurations for better monitoring and management. For added security, the role supports SSL/TLS for this interface. Administrators can specify a domain that will be used to generate the PEM file containing the required keys and certificates, ensuring secure access to the web interface.

The role's flexibility empowers administrators to tailor HAProxy according to their specific requirements, making it a versatile solution for load balancing in diverse application environments. With the role's capability to handle old configurations and provide a customizable setup, administrators can confidently deploy a robust and high-performance load balancer tailored to their needs.

In summary, the HAProxy role streamlines the installation and configuration of the load balancer, while also offering features such as managing old configurations, customizable settings, error page handling, and SSL/TLS support for the web interface. By leveraging this role, administrators can deploy a reliable and efficient HAProxy setup, ensuring smooth traffic distribution and enhanced application performance.

## Usage

```SHELL
# Install husky and init
npm i && npx husky init && npm i validate-branch-name && npm cz

# Initialize the local secrets database, if not already present
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' > .secrets.baseline

# Decrypt the local Ansible Vault
# Get the Ansible Local Vault Key on PassBolt and create the .ansible-vault.key file, after that you can unlock your local vault
MSYS_NO_PATHCONV=1 docker run --rm -it -v "$HOME/.docker:/root/.docker" -v "$(pwd):/root/ansible/${DIR}" -w /root/ansible/${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml'
```

### Linters

```SHELL
# Analyse the project for secrets, and audit
./detect-secrets

# Lint Markdown
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest markdownlint './**.md' --disable MD013

# Lint YAML
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest yamllint -c ./.yamllint .

# Lint Ansible
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest ansible-lint --offline --exclude node_modules -x meta-no-info -p .
```

### Local Tests

This repository contain a unique Molecule controller to start containers and tests your roles and playbook inside them.

```SHELL
# Start the Molecule controller
./molecule-controller

# Inside the controller
molecule create
molecule converge
molecule verify
molecule test
```

### Use

In order to use this role, you have to know its defaults vars and their purppose.

### Vars

This role have these default vars here: [defaults.yml](./defaults/main.yml).

### Import and Run

To run this role or use it inside a playbook, import the call task from [converge.yml](./molecule/default/converge.yml).

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

### 2023-04-27: First Init

* First init of this role with the bootstrap_role playbook by Lord Robin Crombez

### 2023-05-27: Bind / SSL

* Added a test battery to check auth if enabled
* Added a bind address for stats
* Added SSL/TLS handling for stats url

### 2023-04-28: Remove old confs

* Remove all previous conf, ssl and errors if we have to run the install playbook again because: "In general, it is a good practice to only add configurations when they are needed, as this minimizes the chance of errors or conflicts. In your case, it makes sense to install Apache and configure it, but not add any vhosts until they are needed by the app. However, you should also consider how to handle the scenario where you need to reinstall Apache. If you reinstall Apache without cleaning up the old vhosts, you may end up with conflicting configurations, which could cause issues with your app or even your entire system." ChatGPT.

### 2023-05-30: Cryptographic update

* SSL/TLS Materials are not handled by the role
* Certs/CA have to be installed previously/after this role use

### 2023-10-06: New CICD, new Images

* New CI/CD scenario name
* Molecule now use remote Docker image by Lord Robin Crombez
* Molecule now use custom Docker image in CI/CD by env vars
* New CICD with needs and optimization

### 2023-12-14: System users

* Role can now use system users and address groups

### 2024-02-22: New support

* New CI
* Added Sonarqube
* Added support for Debian 12/11
* Added support for Ubuntu 22
* Added random for cluster services

### 2024-05-19: New CI

* Added Markdown lint to the CICD
* Rework all Docker images
* Change CICD vars convention
* New workers
* Removed all automation based on branch

### 2024-10-16: Global refacto

* New hooks
* New var naming
* Refactor the whole role

### 2024-12-31: New CICD and images

* Edited all Docker images
* Rework on the CICD
* Enabled SonarQube

### 2025-01-05: Certificates update

* Edit for use the latest version of import_certificates

## Authors

* Lord Robin Crombez

## Sources

* [Ansible role documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
* [Ansible Molecule documentation](https://molecule.readthedocs.io/)
