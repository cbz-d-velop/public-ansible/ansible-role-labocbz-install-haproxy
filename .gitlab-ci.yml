---

variables:
  ANSIBLE_FORCE_COLOR: "true"
  PY_COLORS: "1"
  #
  DOCKER_REGISTRY: "docker-registry.nexus3.labo-cbz.net"
  DOCKER_IMAGE__DEVOPS_SONAR_SCANNER: "${DOCKER_REGISTRY}/labocbz/devops-sonar-scanner:latest"
  DOCKER_IMAGE__DEVOPS_LINTERS: "${DOCKER_REGISTRY}/labocbz/devops-linters:latest"
  DOCKER_IMAGE__DEVOPS_ANSIBLE_MOLECULE: "${DOCKER_REGISTRY}/labocbz/ansible-molecule:latest"
  DOCKER_IMAGE__ALPINE: "${DOCKER_REGISTRY}/alpine:latest"

stages:
  - "gitlab-test"
  - "lint"
  - "sonarqube"
  - "test"

###############################################################################
#################### EXTENDABLES
###############################################################################

.test: &test
  image: "${DOCKER_IMAGE__DEVOPS_ANSIBLE_MOLECULE}"
  stage: "test"
  before_script:
    - "mkdir -p ~/.docker"
    - "echo $DOCKER_AUTH_CONFIG_BASE64 | base64 -d > ~/.docker/config.json"
    - "docker login"
    - "docker login $DOCKER_REGISTRY"
    - "python3 -m compileall /usr/lib/python3 -qq"
    - "cp ./.ansible.cfg $HOME/.ansible.cfg"
    - "ansible-vault decrypt --vault-password-file ./.ansible-vault.key ./tests/inventory/group_vars/vault.yml"
    - "ansible-galaxy install -r ./molecule/$SCENARIO_NAME/requirements.yml --roles-path=./molecule/$SCENARIO_NAME/roles"
  script:
    - "molecule test --scenario-name $SCENARIO_NAME"
  allow_failure: false
  needs:
    - "sonarqube"
  rules:
    - if: '$CI_COMMIT_MESSAGE=~/^[^Merge]/ && $CI_COMMIT_BRANCH == "develop"'

###############################################################################
#################### CI JOBS
###############################################################################

############################################
#### gitlab-test
############################################

# As a GitLab ci, we use the default template :)
sast:
  stage: "gitlab-test"
include:
  - template: "Security/SAST.gitlab-ci.yml"

############################################
#### lint
############################################

lint:yamls:
  stage: "lint"
  image: "${DOCKER_IMAGE__DEVOPS_LINTERS}"
  script:
    - "ansible-vault decrypt --vault-password-file ./.ansible-vault.key ./tests/inventory/group_vars/vault.yml"
    - "yamllint -c ./.yamllint ."
  allow_failure: false
  rules:
    - if: '$CI_COMMIT_MESSAGE=~/^[^Merge]/ && $CI_COMMIT_BRANCH == "develop"'
    - if: '$CI_COMMIT_BRANCH == "main"'

lint:jinjas:
  stage: "lint"
  image: "${DOCKER_IMAGE__DEVOPS_LINTERS}"
  script:
    - "[ -n \"$(find ./templates -name '*.j2')\" ] && j2lint ./templates/*.j2 --ignore S3 S7 || echo 'No Jinja2 templates found.'"
  allow_failure: false
  rules:
    - if: '$CI_COMMIT_MESSAGE=~/^[^Merge]/ && $CI_COMMIT_BRANCH == "develop"'
    - if: '$CI_COMMIT_BRANCH == "main"'

lint:ansibles:
  stage: "lint"
  image: "${DOCKER_IMAGE__DEVOPS_LINTERS}"
  script:
    - "ansible-vault decrypt --vault-password-file ./.ansible-vault.key ./tests/inventory/group_vars/vault.yml"
    - "ansible-lint --offline --exclude node_modules -x meta-no-info -p ."
  allow_failure: false
  rules:
    - if: '$CI_COMMIT_MESSAGE=~/^[^Merge]/ && $CI_COMMIT_BRANCH == "develop"'
    - if: '$CI_COMMIT_BRANCH == "main"'

lint:markdowns:
  stage: "lint"
  image: "${DOCKER_IMAGE__DEVOPS_LINTERS}"
  script:
    - "markdownlint './README.md' --disable MD013"
  allow_failure: false
  rules:
    - if: '$CI_COMMIT_MESSAGE=~/^[^Merge]/ && $CI_COMMIT_BRANCH == "develop"'
    - if: '$CI_COMMIT_BRANCH == "main"'

lint:secrets:
  stage: "lint"
  image: "${DOCKER_IMAGE__DEVOPS_LINTERS}"
  script:
    - "detect-secrets scan --exclude-files 'node_modules' --baseline .secrets.baseline"
    - "detect-secrets audit .secrets.baseline"
  allow_failure: false
  rules:
    - if: '$CI_COMMIT_MESSAGE=~/^[^Merge]/ && $CI_COMMIT_BRANCH == "develop"'
    - if: '$CI_COMMIT_BRANCH == "main"'

############################################
#### sonarqube
############################################

sonarqube:
  stage: "sonarqube"
  image:
    name: "${DOCKER_IMAGE__DEVOPS_SONAR_SCANNER}"
    entrypoint: [""]
  needs:
    - "lint:yamls"
    - "lint:jinjas"
    - "lint:ansibles"
    - "lint:markdowns"
    - "lint:secrets"
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - ".sonar/cache"
  script:
    - "sonar-scanner"
  rules:
    - if: '$CI_COMMIT_MESSAGE=~/^[^Merge]/ && $CI_COMMIT_BRANCH == "develop"'
    - if: '$CI_COMMIT_BRANCH == "main"'

############################################
#### tests
############################################

molecule:debian-12:
  <<: *test
  variables:
    SCENARIO_NAME: "cicd-debian-12"
  rules:
    - if: '$CI_COMMIT_MESSAGE=~/^[^Merge]/ && $CI_COMMIT_BRANCH == "develop"'

molecule:ubuntu-24:
  <<: *test
  variables:
    SCENARIO_NAME: "cicd-ubuntu-24"
  rules:
    - if: '$CI_COMMIT_MESSAGE=~/^[^Merge]/ && $CI_COMMIT_BRANCH == "develop"'
